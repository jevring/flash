/*
Copyright (c) 2014, Markus Jevring <markus@jevring.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <windows.h>
#include <tlhelp32.h>

DWORD topLevelPid;

// For unknown reasons, when we do "return 1;" or "return 0;" from a method, the whole fucking thing shuts down.
// As a result, we have to define our own binary values.
// This also makes if-statements look a bit strange, but there's nothing we can do
#define WTF_TRUE 12
#define WTF_FALSE 11

// check if the window is visible.
int IsWindowVisibleHack(HWND window) {
	// strangely, just calling IsWindowVisible() from where we want it doesn't work. 
	// Apparently I'm not the only one with this issue: http://forums.codeguru.com/showthread.php?513417-RESOLVED-IsWindowVisible-crashes-application&p=2019547#post2019547
	// That thread has a different solution which, it turns out, DOESN'T work in my case. No idea why.
	if (IsWindowVisible(window)) {
		return WTF_TRUE;
	} else {
		return WTF_FALSE;
	}
}

// flash a window in the task bar
void flash(HWND window) {
	FLASHWINFO flash;
	flash.cbSize = sizeof(FLASHWINFO);
	flash.hwnd = window;
	//flash.dwFlags = FLASHW_TIMER|FLASHW_TRAY; // (or FLASHW_ALL to flash and if it is not minimized)
	flash.dwFlags = FLASHW_ALL; // (or FLASHW_ALL to flash and if it is not minimized)
	flash.uCount = 3;
	flash.dwTimeout = 500;
	
	FlashWindowEx(&flash);
}

// we must have the CALLBACK here, otherwise the type is wrong
BOOL CALLBACK FindOwnerWindow(HWND window, LPARAM lparam) {
	DWORD windowPid; // pointer to the dword holding the window pid of this window
	DWORD threadId = GetWindowThreadProcessId(window, &windowPid);
	if (topLevelPid == windowPid) {
		if (IsWindowVisibleHack(window) == WTF_TRUE) {
			flash(window);
		}
	}
}

DWORD FindTopLevelPid(DWORD lPid, DWORD previousPid) {
	// we carry the previous pid in to avoid having to open the CURRENT process 
	// to check the image name before we recurse

	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (snapshot == INVALID_HANDLE_VALUE) {
		// we've reached the end
		return 0;
	}
	PROCESSENTRY32 pe = {0};
	pe.dwSize = sizeof(PROCESSENTRY32);
	DWORD parentPid = 0;
	
	if (Process32First(snapshot, &pe)) {
		do {
			if (lPid == pe.th32ProcessID) {
				// make the name lower-case and then do the comparison.
				// this updates the memory of the structure, but it's a snapshot, so we don't care.
				// looks hacky, but is actually quite elegant.
				// from here: https://stackoverflow.com/questions/2661766/c-convert-a-mixed-case-string-to-all-lower-case
				char* p = pe.szExeFile;
				for ( ; *p; ++p) *p = tolower(*p);
				
				if (strcmp("explorer.exe", pe.szExeFile) == 0) {
					// this is our stop condition
					break;
				}
				// we found a pid that matches the one we passed. 
				parentPid = pe.th32ParentProcessID;
				break;
			}
		} while (Process32Next(snapshot, &pe));
	}
	CloseHandle(snapshot);
	if (parentPid == 0) {
		return previousPid;
	} else {
		return FindTopLevelPid(parentPid, lPid);
	}
}

int main() {
	DWORD pid = GetCurrentProcessId();
	// find the pid of the root process under explorer.exe that this child process belongs to
	topLevelPid = FindTopLevelPid(pid, -1);
	// flash the matching window. Should ideally only be one
	EnumWindows(FindOwnerWindow, topLevelPid);
	return 0;
}

/*
This code is cobbled together from 
http://support.microsoft.com/kb/124103 
and 
http://stackoverflow.com/questions/73162/how-to-make-the-taskbar-blink-my-application-like-messenger-does-when-a-new-mess
and 
http://forums.codeguru.com/showthread.php?392273-RESOLVED-How-to-get-window-s-HWND-from-it-s-process-handle
and a slew of other sources on the internet
*/
